# MathDemo

MathDemo is a toy project to do mathematical presentation as a slides show, based on an XML file.

It is written in C++, and based on Qt and on Qwt library. Formulas are expressed in MathML language.

For the moment, the application always reads the data from a file named `demo.xml`.

To compile the project:
* install Qt development libraries
* install Qwt development libraries (on Ubuntu: `libqwt-qt5-dev` and `libqwtmathml-qt5-dev`)
* type `qmake` and `make`


## Elements

### demo

`<demo>` is highest level element.

Contained elements:
* `<show>`: set of objects to show at once.


### show

`<show>` defines a set of objects to show at once.

For the moment, the cartesian coordinates in two dimensions are hardcoded between
`-10.0` and `10.0` in x direction, and adapt to window's dimension in y direction.

Contained elements:
* `<formula>`: mathematical formula
* `<image>`: image
* `<text>`: ordinary text
* `<vector2d>`: 2D vector

Optional attributes:
* `clean="yes"`: clean up the screen before showing the objects


### formula

`<formula>` defines a mathematical formula.

Contained elements:
* `<math>`: element that transcribes the formula in MathML markup

Mandatory attributes:
* `x="0.0" y="4.5"`: position of the middle of the formula in cartesian coordinates

Optional attributes:
* `color="green"`: the color used to display the formula; defaults to `black`
* `size="10"`: the font size in points used to display the formula; defaults to `14`


### image

`<image>` defines an image in Scalable Vector Graphics (SVG) format.

Contained elements:
_(none)_

Mandatory attributes:
* `path="my_image.svg"`: path to the image file
* `x="5.0" y="-1.4"`: position of the middle of the image in cartesian coordinates


### text

`<text>` defines some ordinary text.

Contained data:
* The text

Mandatory attributes:
* `x="-2.0" y="3.8"`: position of the middle of the text in cartesian coordinates

Optional attributes:
* `color="red"`: the color used to display the text; defaults to `black`
* `size="20"`: the font size in points used to display the text; defaults to `14`


### vector2d

`<vector2d>` defines a two-dimension vector represented as an arrow.

Contained elements:
_(none)_

Mandatory attributes:
* `x1="-1.0" y1="-2.5"`: origin of the vector in cartesian coordinates
* `x2="1.0" y2="2.5"`: end of the vector in cartesian coordinates

Optional attributes:
* `color="magenta"`: the color used to display the arrow; defaults to `black`
* `thickness="1"`: the thickness in pixels of the arrow's shaft; defaults to `2`
* `length="20"`: the length in pixels of the arrow's head; defaults to `24`
* `width="10"`: the width in pixels of the arrow's head; defaults to `12`
