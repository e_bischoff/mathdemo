#define WIDTH 1280
#define HEIGHT 720

#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QKeyEvent>

#include "board.h"
#include "formula.h"
#include "image.h"
#include "text.h"
#include "vector2d.h"

void DemoBoard::error(const QString &message)
{
  QMessageBox::information(this, QString("demo.xml"), message);
}

QColor DemoBoard::optionalColor(const QDomElement *tag, const char *name, const char *defaultValue) const
{
  QString value(tag->attribute(name, defaultValue));
  QColor color;

  color.setNamedColor(value);

  return color;
}

bool DemoBoard::optionalBool(const QDomElement *tag, const char *name, const char *defaultValue) const
{
  QString value(tag->attribute(name, defaultValue));
  bool boolean = false;

  if (!value.compare("true") || !value.compare("yes"))
    boolean = true;

  return boolean;
}

int DemoBoard::optionalInt(const QDomElement *tag, const char *name, const char *defaultValue) const
{
  QString value(tag->attribute(name, defaultValue));

  return value.toDouble();
}

double DemoBoard::mandatoryDouble(const QDomElement *tag, const char *name)
{
  QString value(tag->attribute(name));

  if (value.isNull())
  {
    error(QString("Missing floating point number %1").arg(name));
    return 0.0;
  }
  return value.toDouble();
}

const QString DemoBoard::mandatoryString(const QDomElement *tag, const char *name)
{
  QString value(tag->attribute(name));

  if (value.isNull())
    error(QString("Missing text string %1").arg(name));

  return value;
}

void DemoBoard::formula(const QDomElement *tag)
{
  QString mathml;
  QTextStream stream(&mathml);
  int size;
  QColor color;
  double x, y;

  tag->firstChildElement().save(stream, 2);
  color = optionalColor(tag, "color", "black");
  size = optionalInt(tag, "size", "14");
  x = mandatoryDouble(tag, "x");
  y = mandatoryDouble(tag, "y");

  new DemoFormula(this, mathml,
                  size, color, x, y);
}

void DemoBoard::image(const QDomElement *tag)
{
  QString path;
  double x, y;

  path = mandatoryString(tag, "path");
  x = mandatoryDouble(tag, "x");
  y = mandatoryDouble(tag, "y");

  new DemoImage(this, path,
                x, y);
}

void DemoBoard::text(const QDomElement *tag)
{
  QString text;
  int size;
  QColor color;
  double x, y;

  text = tag->firstChild().toCharacterData().data();
  color = optionalColor(tag, "color", "black");
  size = optionalInt(tag, "size", "14");
  x = mandatoryDouble(tag, "x");
  y = mandatoryDouble(tag, "y");

  new DemoText(this, text,
               size, color, x, y);
}

void DemoBoard::vector2d(const QDomElement *tag)
{
  QColor color;
  int thickness, length, width;
  double x1, y1, x2, y2;

  color = optionalColor(tag, "color", "black");
  thickness = optionalInt(tag, "thickness", "2");
  length = optionalInt(tag, "length", "24");
  width = optionalInt(tag, "width", "12");
  x1 = mandatoryDouble(tag, "x1");
  y1 = mandatoryDouble(tag, "y1");
  x2 = mandatoryDouble(tag, "x2");
  y2 = mandatoryDouble(tag, "y2");
 
  new DemoVector2d(this, color,
                   thickness, length, width,
                   x1, y1, x2, y2);
}

void DemoBoard::draw()
{
  bool clean;

  clean = optionalBool(&element, "clean", "no");
  if (clean)
  {
    QList<QWidget *> widgets = findChildren<QWidget *>("", Qt::FindDirectChildrenOnly);

    while (widgets.count())
      delete widgets.takeFirst();
  }

  for (QDomElement tag = element.firstChildElement();
       !tag.isNull();
       tag = tag.nextSiblingElement())
  {
    QString name(tag.tagName());

    if (!name.compare("formula"))
      formula(&tag);
    else if (!name.compare("image"))
      image(&tag);
    else if (!name.compare("text"))
      text(&tag);
    else if (!name.compare("vector2d"))
      vector2d(&tag);
    else error(QString("Unknown name:\n\n%1").arg(name));
  }
}

void DemoBoard::step()
{
  if (element.isNull())
  {
    error(QString("End of demo"));
    exit(0);
  }

  QString name(element.tagName());

  if (!name.compare("show"))
    draw();
  else error(QString("Unknown name:\n\n%1").arg(name));

  element = element.nextSiblingElement();
}

void DemoBoard::mouseReleaseEvent(QMouseEvent *)
{
  step();
}

void DemoBoard::keyPressEvent(QKeyEvent *event)
{
  if (event->key() == Qt::Key_Escape)
  {
    error(QString("End of demo"));
    exit(0);
  }
  step();
}

DemoBoard::DemoBoard()
  : QWidget(),
    xmin(-10.0), xmax(10.0), ymin((-10.0 * HEIGHT) / WIDTH), ymax((10.0 * HEIGHT) / WIDTH),
    domDocument(), element()
{
  setGeometry(0, 0, WIDTH, HEIGHT);
  setPalette(QPalette(Qt::white));
}

void DemoBoard::start()
{
  QFile file("demo.xml");
  QString errorStr;
  int errorLine;
  int errorColumn;
  QDomElement root;

  if (!domDocument.setContent(&file, true, &errorStr, &errorLine, &errorColumn))
    error(QString("Parse error at line %1, column %2:\n\n%3").arg(errorLine).arg(errorColumn).arg(errorStr));

  root = domDocument.documentElement();
  element = root.firstChildElement();
}

int DemoBoard::x2X(float x) const
{
  double a = double(WIDTH - 1) / (xmax - xmin);

  return int((x - xmin) * a);
}

int DemoBoard::y2Y(float y) const
{
  double a = double(HEIGHT - 1) / (ymax - ymin);

  return int((ymax - y) * a);
}
