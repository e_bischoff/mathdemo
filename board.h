#ifndef DEMO_BOARD_H
#define DEMO_BOARD_H

#include <QWidget>
#include <QDomDocument>

class DemoBoard : public QWidget
{
  private:
    void error(const QString &message);

    QColor optionalColor(const QDomElement *tag, const char *name, const char *defaultValue) const;
    bool optionalBool(const QDomElement *tag, const char *name, const char *defaultValue) const;
    int optionalInt(const QDomElement *tag, const char *name, const char *defaultValue) const;
    double mandatoryDouble(const QDomElement *tag, const char *name);
    const QString mandatoryString(const QDomElement *tag, const char *name);

    void formula(const QDomElement *tag);
    void image(const QDomElement *tag);
    void text(const QDomElement *tag);
    void vector2d(const QDomElement *tag);
    void draw();
    void step();

  private:
    double xmin, xmax, ymin, ymax;
    QDomDocument domDocument;
    QDomElement element;

  protected:
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;

  public:
    DemoBoard();
    void start();

    int x2X(float x) const;
    int y2Y(float y) const;
};

#endif
