TEMPLATE    = app
TARGET      = demo
INCLUDEPATH += . /usr/include/qwt

DEFINES += QT_DEPRECATED_WARNINGS

QT    += core gui widgets xml svg
LIBS  += -L/usr/lib/qwt -lqwt-qt5 -lqwtmathml-qt5

SOURCES += main.cpp board.cpp formula.cpp image.cpp text.cpp vector2d.cpp
