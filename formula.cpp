#include <qwt_text.h>

#include "formula.h"
#include "board.h"

DemoFormula::DemoFormula(DemoBoard *board, const QString &mathml, int fontSize,
                         QColor color, double x, double y)
  : QwtTextLabel(QwtText(mathml), board)
{
  QFont font;
  QSize size;
  QPalette palette;

  font.setPointSize(fontSize);
  setFont(font);

  palette.setColor(QPalette::Text, color);
  setPalette(palette);

  size = sizeHint();
  move(board->x2X(x) - size.width() / 2,
       board->y2Y(y) - size.height() / 2);

  show();
}
