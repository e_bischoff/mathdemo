#ifndef DEMO_FORMULA_H
#define DEMO_FORMULA_H

class DemoBoard;

#include <qwt_text_label.h>

class DemoFormula : public QwtTextLabel
{
  public:
    DemoFormula(DemoBoard *board, const QString &mathml, int fontSize,
                QColor color, double x, double y);
};

#endif
