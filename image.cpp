#include <QPainter>
#include <QSvgRenderer>

#include "image.h"
#include "board.h"

void DemoImage::paintEvent(QPaintEvent *)
{
  QPainter painter(this);

  renderer.render(&painter);
}

DemoImage::DemoImage(DemoBoard *board, const QString &path,
                     double x, double y)
  : QWidget(board),
    renderer()
{
  QSize size;

  renderer.load(path);

  size = renderer.defaultSize();
  resize(size);
printf("%d %d\n", size.width(), size.height());
  move(board->x2X(x) - size.width() / 2,
       board->y2Y(y) - size.height() / 2);

  show();
}
