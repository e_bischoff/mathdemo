#ifndef DEMO_IMAGE_H
#define DEMO_IMAGE_H

#include <QWidget>
#include <QSvgRenderer>

class DemoBoard;

class DemoImage : public QWidget
{
  private:
    QSvgRenderer renderer;

  protected:
    virtual void paintEvent(QPaintEvent *event) override;

  public:
    DemoImage(DemoBoard *board, const QString &path,
              double x, double y);
};

#endif
