#include <QApplication>

#include <qwt_mathml_text_engine.h>
#include <qwt_text.h>

#include "board.h"

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  QwtText::setTextEngine(QwtText::MathMLText, new QwtMathMLTextEngine());

  DemoBoard board;
  board.start();

  board.show();
  return a.exec();
}
