#include "text.h"
#include "board.h"

DemoText::DemoText(DemoBoard *board, const QString &text, int fontSize,
                   QColor color, double x, double y)
  : QLabel(text, board)
{
  QFont font;
  QSize size;
  QPalette palette;

  font.setPointSize(fontSize);
  setFont(font);

  palette.setColor(QPalette::Text, color);
  setPalette(palette);

  size = sizeHint();
  move(board->x2X(x) - size.width() / 2,
       board->y2Y(y) - size.height() / 2);

  show();
}
