#ifndef DEMO_TEXT_H
#define DEMO_TEXT_H

#include <QLabel>

class DemoBoard;

class DemoText : public QLabel
{
  public:
    DemoText(DemoBoard *board, const QString &text, int fontSize,
             QColor color, double x, double y);
};

#endif
