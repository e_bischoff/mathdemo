#include <QPainter>
#include <QPolygon>

#include "vector2d.h"
#include "board.h"

#include <math.h>

int DemoVector2d::min(int a, int b, int c, int d) const
{
  int min = a;

  if (min > b) min = b;
  if (min > c) min = c;
  if (min > d) min = d;

  return min;
}

int DemoVector2d::max(int a, int b, int c, int d) const
{
  int max = a;

  if (max < b) max = b;
  if (max < c) max = c;
  if (max < d) max = d;

  return max;
}

void DemoVector2d::paintEvent(QPaintEvent *)
{
  QPainter painter(this);
  QPen pen(color);

  pen.setWidth(thickness);
  painter.setPen(pen);
  painter.drawLine(x1, y1, x3, y3);

  QPolygon triangle;
  QBrush brush(color);

  triangle << QPoint(x2, y2);
  triangle << QPoint(x4, y4);
  triangle << QPoint(x5, y5);

  painter.setBrush(brush);
  painter.drawPolygon(triangle);
}

DemoVector2d::DemoVector2d(DemoBoard *board, QColor a_color,
                           int a_thickness, int a_length, int a_width,
                           double a_x1, double a_y1, double a_x2, double a_y2)
  : QWidget(board),
    color(a_color), thickness(a_thickness)
{
  int dx, dy;
  double whole, length, width;
  int xmin, ymin, xmax, ymax;
 
  x1 = board->x2X(a_x1); y1 = board->y2Y(a_y1);
  x2 = board->x2X(a_x2); y2 = board->y2Y(a_y2);

  dx = x2 - x1;
  dy = y2 - y1;
  whole = sqrt(dx * dx + dy * dy);
  length = (double) a_length / whole;
  width = (double) a_width / whole;

  x3 = x2 - length * dx;
  y3 = y2 - length * dy;
  x4 = x3 - width * dy / 2.0;
  y4 = y3 + width * dx / 2.0;
  x5 = x3 + width * dy / 2.0;
  y5 = y3 - width * dx / 2.0;

  xmin = min(x1, x2, x4, x5); ymin = min(y1, y2, y4, y5);
  xmax = max(x1, x2, x4, x5); ymax = max(y1, y2, y4, y5);

  setGeometry(xmin, ymin, xmax - xmin, ymax - ymin);

  x1 -= xmin; y1 -= ymin;
  x2 -= xmin; y2 -= ymin;
  x3 -= xmin; y3 -= ymin;
  x4 -= xmin; y4 -= ymin;
  x5 -= xmin; y5 -= ymin;

  show();
}
