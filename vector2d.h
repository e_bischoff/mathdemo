#ifndef DEMO_VECTOR2D_H
#define DEMO_VECTOR2D_H

#include <QWidget>

class DemoBoard;

class DemoVector2d : public QWidget
{
  private:
    int min(int a, int b, int c, int d) const;
    int max(int a, int b, int c, int d) const;

  private:
    QColor color;
    int thickness;
    int x1, y1, // start
        x2, y2, // end
        x3, y3, // base of arrow
        x4, y4, // corner of arrow
        x5, y5; // other corner of arrow

  protected:
    virtual void paintEvent(QPaintEvent *event) override;

  public:
    DemoVector2d(DemoBoard *board, QColor a_color,
                 int a_thickness, int a_length, int a_width,
                 double a_x1, double a_y1, double a_x2, double a_y2);

};

#endif
